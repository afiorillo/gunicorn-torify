#!/usr/bin/env bash
# builds the package and publishes to PyPI
# must be run from the project root

# increment version
echo "__version__ = \"$(date -u +"%y.%m.%d%H")\"" > gunicorn_torify/_version.py
# build
python setup.py sdist bdist_wheel
# publish
twine upload dist/*
