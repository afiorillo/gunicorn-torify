#!/usr/bin/env sh
# builds the package and publishes the examples to DockerHub
# must be run from the project root and must be logged in

# 
# build first
docker build . -f ./examples/fastapi/Dockerfile -t $DOCKERHUB_USERNAME/gunicorn-torify:fastapi
docker build . -f ./examples/flask/Dockerfile -t $DOCKERHUB_USERNAME/gunicorn-torify:flask
# # push them
docker push $DOCKERHUB_USERNAME/gunicorn-torify:fastapi
docker push $DOCKERHUB_USERNAME/gunicorn-torify:flask
