#!/usr/bin/env bash
# cleans up cache files and leftovers from tests
# must be run from the project root

rm -rf secrets
rm -rf torrc
rm -rf build/
rm -rf dist/
rm -rf .pytest_cache/
rm -rf gunicorn_torify.egg-info/
find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf
