from pathlib import Path
from shutil import which
import stat
from subprocess import Popen, PIPE
from time import sleep, time

from gunicorn_torify.config import TORRC_PATH, TOR_SERVICE_DIR


def is_tor_installed():
    return which("tor") is not None


def make_torrc(
    backend_port: int,
    backend_bind: str = "127.0.0.1",
    secrets_dir: Path = TOR_SERVICE_DIR,
    public_port: int = 80,
):
    # ensure secrets dir exists with right perms
    try:
        secrets_dir.mkdir(parents=True, exist_ok=False)
    except OSError:
        pass  # won't mess with permissions if it exists
    else:
        secrets_dir.chmod(stat.S_IRWXU)
    # now the config file
    fp = TORRC_PATH
    fp.parent.mkdir(parents=True, exist_ok=True)
    fp.write_text(
        """\
HiddenServiceDir {secrets_dir}
HiddenServicePort {public_port} {backend_bind}:{backend_port}
""".format(
            secrets_dir=str(secrets_dir.resolve()),
            public_port=public_port,
            backend_bind=backend_bind,
            backend_port=backend_port,
        )
    )
    return fp


class TorService:
    def __init__(self):
        self.cmd = None

    def start(self, service_dir=None):
        cmd = Popen([which("tor"), "-f", str(TORRC_PATH)], stdout=PIPE, stderr=PIPE)
        self.cmd = cmd
        if not service_dir:
            service_dir = TOR_SERVICE_DIR
        return self.get_hostname(service_dir)

    def get_hostname(self, tor_service_dir, timeout=10):
        start = time()
        fp = Path(tor_service_dir).joinpath("hostname")
        while (time() - start) < timeout:
            try:
                return fp.read_text().strip()
            except OSError:
                # not there yet
                sleep(1)
        raise RuntimeError("Unable to get Hostname from tor")

    def kill(self, cmd=None):
        if not cmd:
            cmd = self.cmd
        cmd.terminate()
        return cmd.communicate()
