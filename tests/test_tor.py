from pathlib import Path
from re import match

import psutil
import pytest

from gunicorn_torify.tor import utils


def test_happy_tor_service():
    tor = utils.TorService()

    # prep torrc
    fp = utils.make_torrc(8080)
    # start it, ensure it's running
    url = tor.start()
    assert match(r".*\.onion", url)
    pid = tor.cmd.pid
    tor_proc = psutil.Process(pid)
    assert Path(tor_proc.exe()).stem == "tor"
    # kill it, ensure it's stopped
    tor.kill()
    with pytest.raises(psutil.NoSuchProcess):
        assert tor_proc.status() == psutil.STATUS_DEAD
    # cleanup
    fp.unlink()
