from fastapi import FastAPI

app = FastAPI()


@app.get("/_ok")
def healthcheck():
    return {"success": True}
