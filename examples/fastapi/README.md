# Example: FastAPI + Uvicorn (through Gunicorn)

[FastAPI](https://fastapi.tiangolo.com/) is modern ASGI web framework that's similar to Flask.

# Usage

1. Ensure `docker` is installed and configured correctly
2. Execute `cd examples/fastapi/ && ./run.sh`
