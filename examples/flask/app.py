from flask import Flask, jsonify

app = Flask(__name__)


@app.route("/_ok")
def healthcheck():
    return jsonify({"success": True})
