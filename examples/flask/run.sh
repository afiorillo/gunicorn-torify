#!/usr/bin/env sh

# build it
echo "Building..."
IMAGE_ID=$( docker build ../.. -f Dockerfile | sed -n 's/^Successfully built \([0-9a-f]*\).*/\1/p')
echo "Built $IMAGE_ID"

# run it
echo "Running $IMAGE_ID"
echo "Use CTRL+C to escape (and clean up)"
docker run --rm $IMAGE_ID
echo "Exited Docker"

# clean up after it's been killed
echo "Cleaning $IMAGE_ID"
docker rmi $IMAGE_ID > /dev/null
